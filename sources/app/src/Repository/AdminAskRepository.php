<?php

namespace App\Repository;

use App\Entity\AdminAsk;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdminAsk|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminAsk|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminAsk[]    findAll()
 * @method AdminAsk[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminAskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminAsk::class);
    }

    // /**
    //  * @return AdminAsk[] Returns an array of AdminAsk objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdminAsk
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
