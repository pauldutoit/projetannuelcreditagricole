<?php

namespace App\Repository;

use App\Entity\Weedz;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Weedz|null find($id, $lockMode = null, $lockVersion = null)
 * @method Weedz|null findOneBy(array $criteria, array $orderBy = null)
 * @method Weedz[]    findAll()
 * @method Weedz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeedzRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Weedz::class);
    }

    // /**
    //  * @return Weedz[] Returns an array of Weedz objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Weedz
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByChildId($child_id): ?Weedz
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.child_id = :val')
            ->setParameter('val', $child_id)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
