<?php
namespace App\Repository;

use App\Entity\Article;
use App\Entity\Virement;
use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class ArticleRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findLastArticles(){
        return $this->createQueryBuilder('a')
            ->orderBy('a.createdAt', 'ASC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findArticlesByCategory($categoryId){
        return $this->createQueryBuilder('a')
            ->where('a.category = :val')
            ->setParameter(':val', $categoryId)
            ->orderBy('a.createdAt', 'ASC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
            ;
    }
}
?>