<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ArticleApproval;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class ArticleApprovalController extends AbstractController
{

    /**
     * @param Request $request
     * @Route("article/approval/validate", name="article_approval_validate")
     * @return Response
     */
    public function articleApprovalValidate(Request $request)
    {
        $idArticleApproval = $request->query->get('id_articleApproval');
        $idArticle = $request->query->get('id_article');
        $articleApproval = $this->getDoctrine()->getRepository(ArticleApproval::class)->find($idArticleApproval);
        $article = $this->getDoctrine()->getRepository(Article::class)->find($idArticle);

        $articleApproval->setStatus("Validate");
        $article->setArticleApproval($articleApproval);
        $em = $this->getDoctrine()->getManager();
        $em->persist($articleApproval);
        $em->persist($article);
        $em->flush();
        return $this->redirectToRoute('admin', array(
            'crudAction' => 'index',
            'entity' => ArticleApproval::class,
        ));
    }

    /**
     * @param Request $request
     * @Route("article/approval/refused", name="article_approval_refused")
     * @return Response
     */
    public function articleApprovalRefuse(Request $request)
    {
        $idArticleApproval = $request->query->get('id_articleApproval');
        $idArticle = $request->query->get('id_article');

        $articleApproval = $this->getDoctrine()->getRepository(ArticleApproval::class)->find($idArticleApproval);
        $article = $this->getDoctrine()->getRepository(Article::class)->find($idArticle);

        $articleApproval->setStatus("Refused");
        $article->setArticleApproval($articleApproval);
        $em = $this->getDoctrine()->getManager();
        $em->persist($articleApproval);
        $em->persist($article);
        $em->flush();
        return $this->redirectToRoute('admin', array(
            'crudAction' => 'index',
            'entity' => ArticleApproval::class,
        ));
    }
}