<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage(Request $request)
    {

        //var_dump($request->cookies);die;
        if($request->cookies->get('tutoriel')){
            return $this->redirectToRoute("security_login");
        }else{
//            $cookie = new Cookie('tutoriel', 'tutoriel', time() + ( 2 * 365 * 24 * 60 * 60));
//            $response = new Response();
//            $response->headers->setCookie($cookie);
//            $response->send();

            return $this->render('user/tutoriel.html.twig');
        }
    }
}
