<?php

namespace App\Controller\Admin;

use App\Entity\ArticleApproval;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Request;

class ArticleApprovalCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ArticleApproval::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('author'),
            AssociationField::new('article'),
            TextField::new('status'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $validate = Action::new('validate', 'Valider', 'fa fa-check')
            ->linkToRoute('article_approval_validate', function (ArticleApproval $entity) {
                return [
                    'id_articleApproval' => $entity->getId(),
                    'id_article' => $entity->getArticle()->getId(),
                ];
            });

        $refuse = Action::new('refuse', 'Refuser', 'fa fa-times')
            ->linkToRoute('article_approval_refused', function(ArticleApproval $entity){
                return [
                    'id_articleApproval' => $entity->getId(),
                    'id_article' => $entity->getArticle()->getId(),
                ];
            });

        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, $validate)
            ->add(Crud::PAGE_INDEX, $refuse)
            ->disable(Action::DELETE, Action::EDIT, Action::NEW);
    }

}
