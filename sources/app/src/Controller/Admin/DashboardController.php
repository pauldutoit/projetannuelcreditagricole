<?php

namespace App\Controller\Admin;

use App\Entity\AdminAsk;
use App\Entity\Appointment;
use App\Entity\Article;
use App\Entity\ArticleApproval;
use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(AppointmentCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Html')
            ->setTranslationDomain('admin');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::section('Blog');
        yield MenuItem::linkToCrud('Article', 'fa fa-calendar', Article::class);
        yield MenuItem::linkToCrud('Category', 'fa fa-tags', Category::class);

        yield MenuItem::section('Demande');
        yield MenuItem::linkToCrud('Rendez-vous', 'fa fa-calendar', Appointment::class);
        yield MenuItem::linkToCrud("Accès admin", 'fa fa-user', AdminAsk::class);
        yield MenuItem::linkToCrud("Articles en attente", 'fa fa-pause', ArticleApproval::class);


        yield MenuItem::section('Autre');
        yield MenuItem::linkToLogout('Logout', 'fa fa-sign-out');

    }
}