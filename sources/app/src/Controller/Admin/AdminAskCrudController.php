<?php

namespace App\Controller\Admin;

use App\Entity\AdminAsk;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


class AdminAskCrudController extends AbstractCrudController
{

    private $mailer;

    /**
     * AdminAskCrudController constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public static function getEntityFqcn(): string
    {
        return AdminAsk::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('user.email')->setLabel('Email')->hideOnForm(),
            TextField::new('user.username')->setLabel('Username')->hideOnForm(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
//        // this action executes the 'renderInvoice()' method of the current CRUD controller
//        $viewValidate = Action::new('validate', 'Valider', 'fa fa-file-invoice')
//            ->linkToCrudAction('validate');

//        // if the method is not defined in a CRUD controller, link to its route
        $validate = Action::new('validate', 'Valider', 'fa fa-check')
            ->linkToRoute('admin_ask_validate', function (AdminAsk $entity) {
                return [
                    'id_ask' => $entity->getId(),
                    'id_user' => $entity->getUser()->getId(),
                ];
            });

        $refuse = Action::new('refuse', 'Refuser', 'fa fa-times')
            ->linkToRoute('admin_ask_refused', function(AdminAsk $entity){
               return [
                 'id_ask' => $entity->getId(),
                 'id_user' => $entity->getUser()->getId(),
               ];
            });


        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, $validate)
            ->add(Crud::PAGE_INDEX, $refuse)
            ->disable(Action::DELETE, Action::EDIT, Action::NEW);
    }


}
