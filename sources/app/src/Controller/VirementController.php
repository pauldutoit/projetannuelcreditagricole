<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Child;
use App\Entity\Pot;
use App\Form\VirementType;
use App\Repository\ArticleRepository;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\ORM\EntityRepository;
use Stripe\Stripe;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Virement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\NotBlank;

class VirementController extends Controller
{
    /**
     * @Route("/virement/{child_id}", name="child_virement")
     * @param $child_id
     * @return Response
     */
    public function showChildVirement($child_id)
    {
        $virements = $this->getDoctrine()->getRepository(Virement::class)->findVirementsByChild($child_id);
        $data = $this->get('jms_serializer')->serialize($virements, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/child/{child_id}/pot/{pot_id}", name="payment", options={"expose"=true})
     * @param Request $request
     * @return Response
     * @throws \Stripe\Exception\ApiErrorException
     */

    public function payment(Request $request, $child_id, $pot_id){
        $form = $this->get('form.factory')
            ->createNamedBuilder('payment-form')
            ->add('amount', IntegerType::class)
            ->add('comment', TextareaType::class)
            ->add('token', HiddenType::class, [
                'constraints' => [new NotBlank()],
            ])
            ->add('pot', EntityType::class,[
                'class' => Pot::class,
                'query_builder' => function(EntityRepository $er) use ($pot_id) {
                    return $er->createQueryBuilder('p')->where('p.id = :val')->setParameter('val', $pot_id);
                },
                'choice_label' => 'label',
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $child = $this->getDoctrine()->getRepository(Child::class)->find($child_id);
        /** @var Pot $pot */
        $pot = $this->getDoctrine()->getRepository(Pot::class)->find($pot_id);
        $max_amount = $pot->getTotalAmount() - $pot->getCurrentAmount();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $paymentForm = $request->get('payment-form');

                \Stripe\Stripe::setApiKey('sk_test_51H02x2Fp0I3CFkgQeDxn74pdyBz4VRYIYQAZNycR84WiUPlf6cNYjLq2DYOpeUZy2w1tpe3AjbFx4dXqCzsIVPY300EJTSAx59');
                $token = $paymentForm['token'];
                $amount = $paymentForm['amount'];
                $comment = $paymentForm['comment'];
                $charge = \Stripe\Charge::create([
                    'amount' => $amount * 100,
                    'currency' => 'eur',
                    'description' => 'Credit Card',
                    'source' => $token,
                ]);

                $pot->setCurrentAmount($pot->getCurrentAmount() + floatval($amount));
                $virement = new Virement();
                $virement->setRecipient($child);
                $virement->setSender($this->getUser());
                $virement->setAmount($amount);
                $virement->setComment($comment);
                $virement->setDate(new \DateTime('now'));
                $virement->setIsRecurrent(false);
                $em = $this->getDoctrine()->getManager();
                $em->persist($pot);
                $em->persist($virement);
                $em->flush();
                $this->addFlash('success', "+ ".$paymentForm['amount']."€ dans la cagnotte \"".$pot->getLabel()."\" de ".$child->getName()." :)");
                return $this->redirectToRoute("my_space");
            }
        }

        return $this->render('user/payment.html.twig', [
            'form' => $form->createView(),
            'max_amount' => $max_amount,
            'stripe_public_key' => $this->getParameter('stripe_public_key'),
        ]);
    }

    /**
     * @Route("/child/{child_id}/pot/{pot_id}/cashin", name="cash_in", options={"expose"=true} )
     * @param Request $request
     * @param $child_id
     * @param $pot_id
     * @return Response
     */
    public function cashIn(Request $request, $child_id, $pot_id){

        return $this->render('user/cashin.html.twig', []);
    }


//    /**
//     * @Route("/addvirement/{child_id}", options={"expose"=true}, name="add_virement")
//     */
//    public function addVirement($child_id, Request $request){
//
//
//        //// STRIPE //////
//        \Stripe\Stripe::setApiKey('sk_test_51H02x2Fp0I3CFkgQeDxn74pdyBz4VRYIYQAZNycR84WiUPlf6cNYjLq2DYOpeUZy2w1tpe3AjbFx4dXqCzsIVPY300EJTSAx59');
//
//        $intent = \Stripe\PaymentIntent::create([
//            'amount' => 2000,
//            'currency' => 'eur',
//            // Verify your integration in this guide by including this parameter
//            'payment_method_types' => ['card'],
//        ]);
//        /////////////
//
//        $virement = new Virement();
//        $virementForm = $this->createForm(VirementType::class, $virement);
//
//        if($virementForm->isSubmitted() && $virementForm->isValid()){
//
//            $virement->setDate(new \DateTime('now'));
//            $virement->setSender('Maman'); // temporaire
//            $virement->setRecipient($this->getDoctrine()->getRepository(Child::class)->findOneBy($child_id));
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($virement);
//            $em->flush();
//        }
//
//        return $this->render('user/add_virement.html.twig',[
//            'virementForm' => $virementForm->createView(),
//            'child_id', $child_id
//        ]);
//    }

}
