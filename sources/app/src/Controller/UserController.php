<?php

namespace App\Controller;

use App\Entity\AdminAsk;
use App\Entity\Appointment;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Document;
use App\Entity\User;
use App\Entity\Virement;
use App\Entity\Visitor;
use App\Entity\Weedz;
use App\Form\AppointmentType;
use App\Form\ChangePasswordType;
use App\Form\ChildType;
use App\Form\DocumentType;
use App\Form\UserType;
use App\Form\VirementType;
use App\Form\RegistrationType;
use App\Form\VisitorType;
use App\Repository\ChildRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Child;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Date;

class UserController extends AbstractController
{
    private function serialize($data)
    {
        return $this->container->get('jms_serializer')
            ->serialize($data, 'json');
    }

    /**
     * @Route("/registration", name="security_registration")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function registration(Request $request, UserPasswordEncoderInterface $encoder){

        $user = new User();
        $registrationForm = $this->createForm(RegistrationType::class, $user);

        $registrationForm->handleRequest($request);
        if($registrationForm->isSubmitted() && $registrationForm->isValid()){
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setCreatedAt(new \DateTime('now'));
            $user->setRoles(['ROLE_USER']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            if($user->isAdmin){
                $adminAsk = new AdminAsk();
                $adminAsk->setUser($this->getDoctrine()->getRepository(User::class)->find($user->getId()));
                $adminAsk->setStatus("Pending");
                $em->persist($adminAsk);
                $em->flush();
                $this->addFlash('success', "Nos services étudient actuellement votre demande d'accès administrateur, regardez vos mails !");
            }

            return $this->redirectToRoute('security_login');
        }

        return $this->render('user/registration.html.twig', [
            'registrationForm' => $registrationForm->createView(),
        ]);
    }


    /**
     * @Route("/login", name="security_login")
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */

    public function login(Request $request, AuthenticationUtils $authenticationUtils){

        $visitor = new Visitor();
        $appointment = new Appointment();
        $visitorForm = $this->createForm(VisitorType::class, $visitor);
        $appointmentForm = $this->createForm(AppointmentType::class, $appointment);

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if($request->get('username')){
            
        }

        return $this->render('user/login.html.twig',[
            'visitorForm' => $visitorForm->createView(),
            'appointmentForm' => $appointmentForm->createView(),
            'error' => $error,
            'lastUsername' => $lastUsername
        ]);
    }

    /**
     * @Route("/login_visitor", name="security_login_visitor")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function loginVisitor(Request $request){

        $visitor = new Visitor();
        if($request->getMethod() === "POST"){

            $params = $request->request->all();
            if(isset($params)) {
                $visitor->setFirstname($params['visitor']['firstname']);
                $visitor->setLastname($params['visitor']['lastname']);
                $visitor->setEmail($params['visitor']['email']);

                $em = $this->getDoctrine()->getManager();
                $em->persist($visitor);
                $em->flush();
                return $this->redirectToRoute('feed', [], 301);
            }
        }
        return $this->redirectToRoute('security_login', [], 301);
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout(){

    }

    /**
     * @Route("/me", name="my_space")
     */
    public function mySpace(){
        $userId = $this->getUser()->getId();
        $children = $this->getDoctrine()->getRepository(Child::class)->findChildrenByUserId($userId);

        return $this->render('user/my_space.html.twig',[
            'children' => $children
        ]);
    }

    /**
     * @Route("/feed", name="feed")
     */
    public function feed(){
        $categoryNames = $this->getDoctrine()->getRepository(Category::class)->findAllCategory();

        return $this->render('user/feed.html.twig', [
            'categoryNames' => $categoryNames
        ]);
    }


    /**
     * @Route("/child/add", name="add_child")
     */
    public function addChild(Request $request){
        $currentUser = $this->getUser();
        $child = new Child();
        $document = new Document();
        $childForm = $this->createForm(ChildType::class, $child);
        $documentForm = $this->createForm(DocumentType::class, $document);
        $documentForm->handleRequest($request);
        if($documentForm->isSubmitted() && $documentForm->isValid()) {
            $document->setCniParent($this->upload('cni_parent', $documentForm));
            $document->setParentPictureFace($this->upload('parent_picture_face', $documentForm));
            $document->setCniChild($this->upload('cni_child', $documentForm));
            $document->setChildPictureFace($this->upload('child_picture_face', $documentForm));
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $childRequest = $request->get('child');
            $child->setName($childRequest['name']);
            $child->setUser($this->getUser());
            // create row for weedz in db
            $child->setDocument($document);
            $em->persist($child);
            $em->flush();
            $weedz = new Weedz();
            $weedz->setChildId($this->getDoctrine()->getRepository(Child::class)->find($child->getId()));
            $weedz->setTotalCount(0.00);
            $weedz->setLastCollect(new \DateTime('now'));
            $em->persist($weedz);
            $em->flush();

            $this->addFlash('success', 'Enfant Ajouté !');
            return $this->redirectToRoute('my_space');
        }

        return $this->render('user/add_child.html.twig',[
            'childForm' => $childForm->createView(),
            'documentForm' => $documentForm->createView()
        ]);
    }

    /**
     * @Route("/appointment/create", name="create_appointment")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function createAppointment(Request $request, \Swift_Mailer $mailer)
    {

        $appointment = $request->get('appointment');
        $appointmentObj = new Appointment();
        $date = new DateTime();
        $appointmentObj->setEmail($appointment['email']);
        $appointmentObj->setFirstname($appointment['firstname']);
        $appointmentObj->setName($appointment['name']);
        $str = $appointment['date']['year']."/".$appointment['date']['month']."/".$appointment['date']['day'];
        $appointmentObj->setDate(new \DateTime($str));

        $em = $this->getDoctrine()->getManager();
        $em->persist($appointmentObj);
        $em->flush();

        $message = (new \Swift_Message('Prise de RDV avec votre conseiller'))
            ->setFrom('no.reply.bankidz@gmail.com')
            ->setTo($appointment['email'])
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'email/appointment.html.twig',
                    ['appointment' => $appointmentObj]
                ),
                'text/html'
            );

        if ($mailer->send($message)) {
            $this->addFlash('success', 'Le mail a bien été envoyé');
        }else{
            $this->addFlash('error', "Une erreur est survenue lors de l'envoie du mail");
        }
        return $this->redirectToRoute('feed');
    }

    /**
     * @Route("/admin/validateAsk", name="admin_ask_validate")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validate(Request $request, \Swift_Mailer $mailer){
        $id_ask = $request->query->get('id_ask');
        $id_user = $request->query->get('id_user');
        $adminAsk = $this->getDoctrine()->getRepository(AdminAsk::class)->find($id_ask);
        $user = $this->getDoctrine()->getRepository(User::class)->find($id_user);
        $user->setRoles(['ROLE_ADMIN']);
        $em = $this->getDoctrine()->getManager();
        $em->remove($adminAsk);
        $em->persist($user);
        $em->flush();
        $message = (new \Swift_Message('Accès Administrateur'))
            ->setFrom('no.reply.bankidz@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'email/adminAskValid.html.twig',
                    ['user' => $user]
                ),
                'text/html'
            );
        if ($mailer->send($message)) {
            $this->addFlash('success', "Le mail notifiant l'utilisateur de votre décision a bien été envoyé");
        }else{
            $this->addFlash('error', "Une erreur est survenue lors de l'envoie du mail");
        }
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/admin/RefusedAsk", name="admin_ask_refused")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function refuse(Request $request, \Swift_Mailer $mailer){
        $id_ask = $request->query->get('id_ask');
        $id_user = $request->query->get('id_user');
        $adminAsk = $this->getDoctrine()->getRepository(AdminAsk::class)->find($id_ask);
        $user = $this->getDoctrine()->getRepository(User::class)->find($id_user);
        $user->setRoles(['ROLE_USER']);
        $em = $this->getDoctrine()->getManager();
        $em->remove($adminAsk);
        $em->persist($user);
        $em->flush();
        $message = (new \Swift_Message('Accès Administrateur'))
            ->setFrom('no.reply.bankidz@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'email/adminAskRefus.html.twig',
                    ['user' => $user]
                ),
                'text/html'
            );
        if ($mailer->send($message)) {
            $this->addFlash('success', "Le mail notifiant l'utilisateur de votre décision a bien été envoyé");
        }else{
            $this->addFlash('error', "Une erreur est survenue lors de l'envoie du mail");
        }
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/user/{user_id}/edit", name="user_edit")
     * @param Request $request
     * @param $user_id
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function userEdit(Request $request, $user_id, UserPasswordEncoderInterface $encoder){
        $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);
        $userForm = $this->createForm(UserType::class, $user);
        $checkPass = $encoder->isPasswordValid($user, $request->get('password'));
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            if($checkPass) {
                $file = $userForm->get('picture')->getData();
                if ($file != null) {
                    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
                    try {
                        $file->move(
                            $this->getParameter('img_directory'),
                            $newFilename
                        );
                    } catch (FileException $e) {

                    }
                    $user->setPicture('/uploads/img/'.$newFilename);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', "Vos informations ont bien été modifiées");
            }else{
                $this->addFlash('error', "Le mot de passe est incorrect");
            }
        }

        return $this->render('user/user_edit.html.twig',[
            'userForm' => $userForm->createView(),
        ]);
    }

    /**
     * @Route("user/{user_id}/changePassword", name="change_password")
     * @param Request $request
     * @param $user_id
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function changePassword(Request $request, $user_id, UserPasswordEncoderInterface $encoder){
        $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);
        $changePasswordForm = $this->createForm(ChangePasswordType::class, $user);
        $changePasswordForm->handleRequest($request);
        $password = $request->get('change_password');
        if($password != null){
            if($encoder->isPasswordValid($user, $password['password'])){
                $em = $this->getDoctrine()->getManager();
                $user->setPassword($encoder->encodePassword($user, $password['new_password']));
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', "Le mot de passe a été changé avec succès");
            }else{
                $this->addFlash('error', "Erreur lors du changement de mot de passe, veuillez réessayer");
            }

        }
        return $this->render('user/changePassword.html.twig', [
            'changePasswordForm' => $changePasswordForm->createView(),
        ]);
    }

    private function upload($field, $form){
        $file = $form->get($field)->getData();
        if ($file) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
            try {
                $file->move(
                    $this->getParameter('img_directory'),
                    $newFilename
                );
            } catch (FileException $e) {

            }
            return '/uploads/img/'.$newFilename;
        }
    }


}

