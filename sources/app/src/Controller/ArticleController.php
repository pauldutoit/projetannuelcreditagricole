<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ArticleApproval;
use App\Entity\ArticleUser;
use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Form\VirementType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller
{
//    /**
//     * @Route("/articles/{id}", name="article_show")
//     */
//    public function showAction()
//    {
//        $article = new Article();
//        $article
//            ->setTitle('Mon premier article')
//            ->setContent('Le contenu de mon article.')
//        ;
//        $data = $this->get('jms_serializer')->serialize($article, 'json');
//
//        $response = new Response($data);
//        $response->headers->set('Content-Type', 'application/json');
//
//        return $response;
//    }

    /**
     * @Route("/article/create", name="article_create")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $article = new Article();
        $articleForm = $this->createForm(ArticleType::class, $article);
        $articleForm->handleRequest($request);
        if($articleForm->isSubmitted() && $articleForm->isValid()){
            $file = $articleForm->get('picture')->getData();
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
                try {
                    $file->move(
                        $this->getParameter('img_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {

                }
                $article->setPicture('/uploads/img/'.$newFilename);
            }

            // creation de l'article
            $article->setAuthor($this->getUser());
            $article->setCreatedAt(new \DateTime('now'));
            $article->setUpdatedAt(new \DateTime('now'));
            $article->setUpCount(0);
            $article->setDownCount(0);

            // article créé par un utilisateur -> demande de création
            $articleApproval = new ArticleApproval();
            $articleApproval->setStatus('Pending');
            $articleApproval->setAuthor($this->getUser());
            $article->setArticleApproval($articleApproval);

            $em = $this->getDoctrine()->getManager();
            $em->persist($articleApproval);
            $em->persist($article);
            $em->flush();

            $this->addFlash('success', "Vous venez de créer un article, celui ci sera visible après sa validation par un membre de notre équipe");
            return $this->redirectToRoute('feed');
        }

            return $this->render('user/article_create.html.twig', [
                'articleForm' => $articleForm->createView()
        ]);
    }


    private function serialize($data)
    {
        return $this->container->get('jms_serializer')
            ->serialize($data, 'json');
    }


    /**
     * @Route("/articles", name="article_list", methods={"GET"} )
     */
    public function listAction()
    {
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        $data = $this->get('jms_serializer')->serialize($articles, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/last_articles", name="last_articles", methods={"GET"})
     */
    public function lastArticleAction(){
        $last_articles = $this->getDoctrine()->getRepository(Article::class)->findLastArticles();
        $data = $this->get('jms_serializer')->serialize($last_articles, 'json');
        $response = new Response($data);

        $response->headers->set('Content-Type','application/json');
        return $response;
    }

    /**
     * @Route("/articles/tag/{category_id}", name="articles_by_category", methods={"GET"})
     * @param Request $request
     * @param $category_id
     * @return Response
     */
    public function listArticleByCategoryAction(Request $request, $category_id){

        $articles = $this->getDoctrine()->getRepository(Article::class)->findArticlesByCategory($category_id);
        $data = $this->serialize($articles);
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/article/{id}", options={"expose"=true}, name="article_show")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function showArticle(Request $request, $id){

        $comment = new Comment();
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

        return $this->render('user/article.html.twig', [
            "article" => $article,
        ]);
    }


    /**
     * @Route("/article/{id}/user")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function bindUserArticle(Request $request, $id){

        $currentUser = $this->getUser();
        $articleUser = $this->getDoctrine()->getRepository(ArticleUser::class)->findByArticleAndUser($currentUser->getId(), $id);

        if(empty($articleUser)){
            $articleUser = new ArticleUser();
            $articleUser->setUser($this->getDoctrine()->getRepository(User::class)->find($currentUser->getId()));
            $articleUser->setArticle($this->getDoctrine()->getRepository(Article::class)->find($id));
            $articleUser->setIsDownCount(false);
            $articleUser->setIsUpCount(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($articleUser);
            $em->flush();
        }
        $data = $this->serialize($articleUser);
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }




    /**
     * @Route("/article/{id}/arrowup", name="article_arrow_up")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function updateArrowUp(Request $request, $id){

        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        $articleUser = $this->getDoctrine()->getRepository(ArticleUser::class)->findByArticleAndUser($this->getUser()->getId(), $id);

        if($articleUser->getIsUpCount() === false) {
            $article->setUpCount($article->getUpCount() + 1);
            $articleUser->setIsUpCount(true);
            if($articleUser->getIsDownCount(true)){
                $articleUser->setIsDownCount(false);
                $article->setDownCount($article->getDownCount() - 1);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->persist($articleUser);
        $em->flush();
        $data = $this->serialize($article);
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/article/{id}/arrowdown", name="article_arrow_down")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function updateArrowDown(Request $request, $id){

        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        $articleUser = $this->getDoctrine()->getRepository(ArticleUser::class)->findByArticleAndUser($this->getUser()->getId(), $id);

        if($articleUser->getIsDownCount() === false){
            $article->setDownCount($article->getDownCount() + 1);
            $articleUser->setIsDownCount(true);
            /** down vote suppress up vote */
            if($articleUser->getIsUpCount(true)){
                $article->setUpCount($article->getUpCount() - 1);
                $articleUser->setIsUpCount(false);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->persist($articleUser);
        $em->flush();
        $data = $this->serialize($article);
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
