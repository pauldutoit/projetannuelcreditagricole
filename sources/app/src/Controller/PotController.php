<?php

namespace App\Controller;

use App\Entity\Child;
use App\Entity\Pot;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\PotType;


class PotController extends Controller
{
    /**
     * @Route("/pots/{child_id}", name="child_pots")
     */
    public function showAction($child_id)
    {
        $pots = $this->getDoctrine()->getRepository(Pot::class)->findPotsByChild($child_id);
        $data = $this->get('jms_serializer')->serialize($pots, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/addpot/{child_id}", options={"expose"=true}, name="add_pot")
     */
    public function addPot($child_id, Request $request){

        $pot = new Pot();
        $potForm = $this->createForm(PotType::class, $pot);
        $child = $this->getDoctrine()->getRepository(Child::class)->find($child_id);
        $potForm->handleRequest($request);

        if($potForm->isSubmitted() && $potForm->isValid()){
            //$pot = $potForm->getData();
            $pot->setCurrentAmount(0);
            $pot->setRecipient($child);
            $em = $this->getDoctrine()->getManager();
            $em->persist($pot);
            $em->flush();
            return $this->redirectToRoute('my_space');
        }

        return $this->render('user/add_pot.html.twig', [
            'potForm' => $potForm->createView()
        ]);
    }

    /**
     * @Route("child/{child_id}/deletePot/{pot_id}", options={"expose"=true}, name="delete_pot")
     * @param $pot_id
     * @param $child_id
     * @param Request $request
     * @return Response|void
     */
    public function deletePot($pot_id, $child_id, Request $request){

        $pot = $this->getDoctrine()->getRepository(Pot::class)->find($pot_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($pot);
        $em->flush();

        $pots = $this->getDoctrine()->getRepository(Pot::class)->findPotsByChild($child_id);
        $data = $this->get('jms_serializer')->serialize($pots, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
