<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Virement;
use App\Form\VirementType;
use App\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Child;


class CommentController extends Controller
{

    /**
     * @Route("/article/{article_id}/post", options={"expose"=true}, name="comment_post")
     * @param Request $request
     * @param $article_id
     * @return Response
     * @throws \Exception
     */

    public function postCommentAction(Request $request, $article_id){

        $comment = new Comment();
        $data = $request->getContent();
        $dataArray = explode("=", urldecode(str_replace("\"", "", $data)));
        $comment->setContent(urldecode($dataArray[1]));
        $comment->setAuthor($this->getUser());
        $comment->setArticle($this->getDoctrine()->getRepository(Article::class)->find($article_id));
        $comment->setPostedAt(new \DateTime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        $data = $this->get('jms_serializer')->serialize($comment, 'json');

        return new Response($data, Response::HTTP_CREATED);

    }

    /**
     * @Route("/article/{article_id}/comment/{comment_id}/delete", options={"expose"=true}, name="delete_comment")
     * @param Request $request
     * @param $article_id
     * @param $comment_id
     * @return Response
     */

    public function deleteCommentAction(Request $request, $article_id, $comment_id){
        $comment = $this->getDoctrine()->getRepository(Comment::class)->find($comment_id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();
        $comments = $this->getDoctrine()->getRepository(Comment::class)->findCommentsByArticleId($article_id);
        $data = $this->get('jms_serializer')->serialize($comments, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type','application/json');
        return $response;
    }

    /**
     * @Route("article/{article_id}/comments", options={"expose"=true}, name="show_comments")
     * @param Request $request
     * @param $article_id
     * @return Response
     */
    public function showCommentByArticleIdAction(Request $request, $article_id){

        $comments = $this->getDoctrine()->getRepository(Comment::class)->findCommentsByArticleId($article_id);
        $data = $this->get('jms_serializer')->serialize($comments, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type','application/json');
        return $response;

    }
}
