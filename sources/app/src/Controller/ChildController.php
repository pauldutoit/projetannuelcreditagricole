<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ArticleUser;
use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Weedz;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Form\VirementType;
use App\Repository\ArticleRepository;
use Safe\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\SerializerBundle\JMSSerializerBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Intl\Timezones;
/**
 * Class ChildController
 * @package App\Controller
 */
class ChildController extends Controller
{

    private function serialize($data)
    {
        return $this->container->get('jms_serializer')
            ->serialize($data, 'json');
    }

    /**
     * @Route("/child/{child_id}/weedz", name="child_weedz")
     */
    public function getWeedzCount($child_id){
        /** @var Weedz $weedz */
        $weedz = $this->getDoctrine()->getRepository(Weedz::class)->findByChildId($child_id);
        $data = $this->serialize($weedz);
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/child/{child_id}/weedz/collect", options={"expose"=true}, name="weedz_collect")
     * @param Request $request
     * @param $child_id
     * @return Response
     * @throws \Exception
     */

    public function weedzCollect(Request $request, $child_id){

        /** @var Weedz $weedz */
        $weedz = $this->getDoctrine()->getRepository(Weedz::class)->findByChildId($child_id);
        $weedz->setLastCollect(new \DateTime());
        $weedz->setTotalCount($weedz->getTotalCount() + intval($request->getContent()));
        $em = $this->getDoctrine()->getManager();
        $em->persist($weedz);
        $em->flush();
        $data = $this->serialize($weedz->getTotalCount());
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}

