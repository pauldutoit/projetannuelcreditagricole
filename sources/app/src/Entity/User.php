<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\EqualTo;
/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(
 *     fields={"email"},
 *     message="Cet email est déjà utilisé"
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    public $new_password;

    /**
     * @Assert\EqualTo(propertyPath="new_password", message="Les deux mots de passe ne sont pas semblable")
     */
    public $password_confirm;

    /**
     * @Assert\EqualTo(propertyPath="password", message="Les deux mots de passe ne sont pas semblable")
     */
    public $password_confirm_registration;


    public $isAdmin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="json")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity=Child::class, mappedBy="user")
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="author")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="author", cascade={"persist"})
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity=ArticleUser::class, mappedBy="user", cascade={"remove"})
     */
    private $articleUsers;

    /**
     * @ORM\OneToMany(targetEntity=ArticleApproval::class, mappedBy="author")
     */
    private $articleApproval;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->articleUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
      return $this->roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }



    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

//    /**
//     * String representation of object
//     * @link https://php.net/manual/en/serializable.serialize.php
//     * @return string the string representation of the object or null
//     * @since 5.1.0
//     */
//    public function serialize()
//    {
//        return $this->serialize([
//           $this->id,
//           $this->username,
//           $this->password
//        ]);
//    }
//
//    /**
//     * Constructs the object
//     * @link https://php.net/manual/en/serializable.unserialize.php
//     * @param string $serialized <p>
//     * The string representation of the object.
//     * </p>
//     * @return void
//     * @since 5.1.0
//     */
//    public function unserialize($serialized)
//    {
//        list(
//                $this->id,
//                $this->username,
//                $this->password
//            ) = $this->unserialize($serialized, [allowed_classes => false]);
//    }


    /**
     * @return Collection|Child[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Child $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setUser($this);
        }

        return $this;
    }

    public function removeChild(Child $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getUser() === $this) {
                $child->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setAuthor($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getAuthor() === $this) {
                $comment->setAuthor(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(Author $author): self
    {
        $this->author = $author;

        // set the owning side of the relation if necessary
        if ($author->getUser() !== $this) {
            $author->setUser($this);
        }

        return $this;
    }

    public function getArticle()
    {
        return $this->article;
    }

    public function setArticle(Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return Collection|ArticleUser[]
     */
    public function getArticleUsers(): Collection
    {
        return $this->articleUsers;
    }

    public function addArticleUser(ArticleUser $articleUser): self
    {
        if (!$this->articleUsers->contains($articleUser)) {
            $this->articleUsers[] = $articleUser;
            $articleUser->setUser($this);
        }

        return $this;
    }

    public function removeArticleUser(ArticleUser $articleUser): self
    {
        if ($this->articleUsers->contains($articleUser)) {
            $this->articleUsers->removeElement($articleUser);
            // set the owning side to null (unless already changed)
            if ($articleUser->getUser() === $this) {
                $articleUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->email;
    }

    public function getArticleApproval(): ?ArticleApproval
    {
        return $this->articleApproval;
    }

    public function setArticleApproval(?ArticleApproval $articleApproval): self
    {
        $this->articleApproval = $articleApproval;

        return $this;
    }
}
