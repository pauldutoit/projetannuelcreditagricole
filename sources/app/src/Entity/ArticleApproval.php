<?php

namespace App\Entity;

use App\Repository\ArticleApprovalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticleApprovalRepository::class)
 */
class ArticleApproval
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="articleApproval")
     */
    private $author;

    /**
     * @ORM\OneToOne(targetEntity=Article::class, inversedBy="articleApproval", cascade={"persist", "remove"})
     */
    private $article;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    public function __construct()
    {
        $this->author = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function addAuthor(User $author)
    {
        if (!$this->author->contains($author)) {
            $this->author[] = $author;
            $author->setArticleApproval($this);
        }

        return $this;
    }

    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    public function removeAuthor(User $author): self
    {
        if ($this->author->contains($author)) {
            $this->author->removeElement($author);
            // set the owning side to null (unless already changed)
            if ($author->getArticleApproval() === $this) {
                $author->setArticleApproval(null);
            }
        }

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function __toString()
    {
        return $this->status;
    }
}
