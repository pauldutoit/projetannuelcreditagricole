<?php

namespace App\Entity;

use App\Repository\ArticleUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticleUserRepository::class)
 */
class ArticleUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="articleUsers")
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="articleUsers")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isUpCount;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isDownCount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsUpCount(): ?bool
    {
        return $this->isUpCount;
    }

    public function setIsUpCount(bool $isUpCount): self
    {
        $this->isUpCount = $isUpCount;

        return $this;
    }

    public function getIsDownCount(): ?bool
    {
        return $this->isDownCount;
    }

    public function setIsDownCount(bool $isDownCount): self
    {
        $this->isDownCount = $isDownCount;

        return $this;
    }
}
