<?php

namespace App\Entity;

use App\Repository\PotRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PotRepository::class)
 */
class Pot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="float")
     */
    private $current_amount;

    /**
     * @ORM\Column(type="float")
     */
    private $total_amount;

    /**
     * @ORM\ManyToOne(targetEntity=Child::class, inversedBy="pots")
     */
    private $recipient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCurrentAmount(): ?float
    {
        return $this->current_amount;
    }

    public function setCurrentAmount(float $current_amount): self
    {
        $this->current_amount = $current_amount;

        return $this;
    }

    public function getTotalAmount(): ?float
    {
        return $this->total_amount;
    }

    public function setTotalAmount(float $total_amount): self
    {
        $this->total_amount = $total_amount;

        return $this;
    }

    public function getRecipient(): ?Child
    {
        return $this->recipient;
    }

    public function setRecipient(?Child $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }
}
