<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @ORM\Table()
 */
class Article
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="User", fetch="EAGER")
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="articles")
     */
    private $category;

    /**
     * @ORM\Column(type="integer")
     */
    private $upCount;

    /**
     * @ORM\Column(type="integer")
     */
    private $downCount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="article", cascade={"remove"})
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=ArticleUser::class, mappedBy="article", cascade={"remove"})
     */
    private $articleUsers;

    /**
     * @ORM\OneToOne(targetEntity=ArticleApproval::class, mappedBy="article", cascade={"persist", "remove"})
     */
    private $articleApproval;


    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->user = new ArrayCollection();
        $this->articleUsers = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getPictureFile(): ?string
    {
        return $this->pictureFile;
    }

    public function setPictureFile(File $pictureFile): self
    {
        $this->pictureFile = $pictureFile;

        if($pictureFile){
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUpCount(): ?int
    {
        return $this->upCount;
    }

    public function setUpCount(int $upCount): self
    {
        $this->upCount = $upCount;

        return $this;
    }

    public function getDownCount(): ?int
    {
        return $this->downCount;
    }

    public function setDownCount(int $downCount): self
    {
        $this->downCount = $downCount;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArticleUser[]
     */
    public function getArticleUsers(): Collection
    {
        return $this->articleUsers;
    }

    public function addArticleUser(ArticleUser $articleUser): self
    {
        if (!$this->articleUsers->contains($articleUser)) {
            $this->articleUsers[] = $articleUser;
            $articleUser->setArticle($this);
        }

        return $this;
    }

    public function removeArticleUser(ArticleUser $articleUser): self
    {
        if ($this->articleUsers->contains($articleUser)) {
            $this->articleUsers->removeElement($articleUser);
            // set the owning side to null (unless already changed)
            if ($articleUser->getArticle() === $this) {
                $articleUser->setArticle(null);
            }
        }

        return $this;
    }

    public function getArticleApproval(): ?ArticleApproval
    {
        return $this->articleApproval;
    }

    public function setArticleApproval(?ArticleApproval $articleApproval): self
    {
        $this->articleApproval = $articleApproval;

        // set (or unset) the owning side of the relation if necessary
        $newArticle = null === $articleApproval ? null : $this;
        if ($articleApproval->getArticle() !== $newArticle) {
            $articleApproval->setArticle($newArticle);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }
}