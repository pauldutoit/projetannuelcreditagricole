<?php

namespace App\Entity;

use App\Repository\DocumentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 */
class Document
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cni_parent;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $parent_picture_face;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cni_child;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $child_picture_face;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCniParent(): ?string
    {
        return $this->cni_parent;
    }

    public function setCniParent(string $cni_parent): self
    {
        $this->cni_parent = $cni_parent;

        return $this;
    }


    public function getParentPictureFace(): ?string
    {
        return $this->parent_picture_face;
    }

    public function setParentPictureFace(string $parent_picture_face): self
    {
        $this->parent_picture_face = $parent_picture_face;

        return $this;
    }

    public function getCniChild(): ?string
    {
        return $this->cni_child;
    }

    public function setCniChild(?string $cni_child): self
    {
        $this->cni_child = $cni_child;

        return $this;
    }

    public function getChildPictureFace(): ?string
    {
        return $this->child_picture_face;
    }

    public function setChildPictureFace(?string $child_picture_face): self
    {
        $this->child_picture_face = $child_picture_face;

        return $this;
    }
}
