<?php

namespace App\Entity;

use App\Repository\WeedzRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass=WeedzRepository::class)
 */
class Weedz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Child::class, cascade={"persist", "remove"})
     */
    private $child_id;

    /**
     * @ORM\Column(type="float")
     */
    private $total_count;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastCollect;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChildId(): ?Child
    {
        return $this->child_id;
    }

    public function setChildId(?Child $child_id): self
    {
        $this->child_id = $child_id;

        return $this;
    }

    public function getTotalCount(): ?float
    {
        return $this->total_count;
    }

    public function setTotalCount(float $total_count): self
    {
        $this->total_count = $total_count;

        return $this;
    }

    public function getLastCollect(): ? \DateTime
    {
        return $this->lastCollect;
    }

    public function setLastCollect(\DateTime $lastCollect): self
    {
        $this->lastCollect = $lastCollect;

        return $this;
    }
}
