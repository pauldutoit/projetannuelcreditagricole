// 1. Basic object for our stuff
window.sliderHomepage = {};

// 2. Settings
sliderHomepage.sliderPanelSelector = '.slider-element-step-page';
sliderHomepage.sliderPaginationSelector = '.slider-pagination-step-page';
sliderHomepage.sensitivity = 5; // horizontal % needed to trigger swipe

// 2. Placeholder to remember which slide we’re on
sliderHomepage.activeSlide = 0;

// 3. Slide counter
sliderHomepage.slideCount = 0;

// 4. Initialization + event listener
sliderHomepage.init = function( selector ) {

    // 4a. Find the container
    sliderHomepage.sliderEl = document.querySelector( selector );

    // 4b. Count stuff
    sliderHomepage.slideCount = sliderHomepage.sliderEl.querySelectorAll( sliderHomepage.sliderPanelSelector ).length;

    // 4c. Populate pagination
    var n = 0;
    for( n; n < sliderHomepage.slideCount; n++ ) {
        var activeStatus = n == sliderHomepage.activeSlide ? ' class="is-active"' : '';
        sliderHomepage.sliderEl.parentElement.querySelector( sliderHomepage.sliderPaginationSelector ).innerHTML += '<div ' + activeStatus + '></div>';
    }

    // 4d. Set up HammerJS
    var sliderManager = new Hammer.Manager( sliderHomepage.sliderEl );
    sliderManager.add( new Hammer.Pan({ threshold: 0, pointers: 0 }) );
    sliderManager.on( 'pan', function( e ) {

        // 4e. Calculate pixel movements into 1:1 screen percents so gestures track with motion
        var percentage = 100 / sliderHomepage.slideCount * e.deltaX / window.innerWidth;

        // 4f. Multiply percent by # of slide we’re on
        var percentageCalculated = percentage - 100 / sliderHomepage.slideCount * sliderHomepage.activeSlide;

        // 4g. Apply transformation
        sliderHomepage.sliderEl.style.transform = 'translateX( ' + percentageCalculated + '% )';

        // 4h. Snap to slide when done
        if( e.isFinal ) {
            if( e.velocityX > 1 ) {
                sliderHomepage.goTo( sliderHomepage.activeSlide - 1 );
            } else if( e.velocityX < -1 ) {
                sliderHomepage.goTo( sliderHomepage.activeSlide + 1 )
            } else {
                if( percentage <= -( sliderHomepage.sensitivity / sliderHomepage.slideCount ) )
                    sliderHomepage.goTo( sliderHomepage.activeSlide + 1 );
                else if( percentage >= ( sliderHomepage.sensitivity / sliderHomepage.slideCount ) )
                    sliderHomepage.goTo( sliderHomepage.activeSlide - 1 );
                else
                    sliderHomepage.goTo( sliderHomepage.activeSlide );
            }
        }
    });
};

// 5. Update current slide
sliderHomepage.goTo = function( number ) {

    // 5a. Stop it from doing weird things like moving to slides that don’t exist
    if( number < 0 )
        sliderHomepage.activeSlide = 0;
    else if( number > sliderHomepage.slideCount - 1 )
        sliderHomepage.activeSlide = sliderHomepage.slideCount - 1;
    else
        sliderHomepage.activeSlide = number;

    // 5b. Apply transformation & smoothly animate via .is-animating CSS
    sliderHomepage.sliderEl.classList.add( 'is-animating' );
    var percentage = -( 100 / sliderHomepage.slideCount ) * sliderHomepage.activeSlide;
    sliderHomepage.sliderEl.style.transform = 'translateX( ' + percentage + '% )';
    clearTimeout( sliderHomepage.timer );
    sliderHomepage.timer = setTimeout( function() {
        sliderHomepage.sliderEl.classList.remove( 'is-animating' );
    }, 400 );

    // 5c. Update the counters
    var pagination = sliderHomepage.sliderEl.parentElement.querySelectorAll( sliderHomepage.sliderPaginationSelector + ' > *' );
    var n = 0;
    for( n; n < pagination.length; n++ ) {
        var className = n == sliderHomepage.activeSlide ? 'is-active' : '';
        pagination[n].className = className;
    }
};

// Initialize
sliderHomepage.init( '.slider-step-page' );