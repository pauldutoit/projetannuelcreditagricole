    /*
  Slidemenu come to left
*/
(function() {
    var $body = document.body
        , $menu_trigger = $body.getElementsByClassName('menu-slide-big')[0];

    var $content_trigger = $body.getElementsByClassName('container-trigger')[0];

    if ( typeof $menu_trigger !== 'undefined' ) {
        $menu_trigger.addEventListener('click', function() {
            $body.className = ( $body.className == 'menu-active' )? '' : 'menu-active';
        });
    }

    if ( typeof $content_trigger !== 'undefined') {
        $content_trigger.addEventListener('click', function () {
            if ($body.className == 'menu-active') {
                $body.className = '';
            }
        })
    }

}).call(this);