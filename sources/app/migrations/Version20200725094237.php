<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200725094237 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_article');
        $this->addSql('ALTER TABLE article_approval ADD author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article_approval ADD CONSTRAINT FK_43086140F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_43086140F675F31B ON article_approval (author_id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64964E69A67');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6497294869C');
        $this->addSql('DROP INDEX IDX_8D93D64964E69A67 ON user');
        $this->addSql('DROP INDEX UNIQ_8D93D6497294869C ON user');
        $this->addSql('ALTER TABLE user DROP article_id, DROP article_approval_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_article (user_id INT NOT NULL, article_id INT NOT NULL, INDEX IDX_5A37106CA76ED395 (user_id), INDEX IDX_5A37106C7294869C (article_id), PRIMARY KEY(user_id, article_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_article ADD CONSTRAINT FK_5A37106C7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_article ADD CONSTRAINT FK_5A37106CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_approval DROP FOREIGN KEY FK_43086140F675F31B');
        $this->addSql('DROP INDEX IDX_43086140F675F31B ON article_approval');
        $this->addSql('ALTER TABLE article_approval DROP author_id');
        $this->addSql('ALTER TABLE user ADD article_id INT DEFAULT NULL, ADD article_approval_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64964E69A67 FOREIGN KEY (article_approval_id) REFERENCES article_approval (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64964E69A67 ON user (article_approval_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497294869C ON user (article_id)');
    }
}
